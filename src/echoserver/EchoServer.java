package echoserver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.StringTokenizer;

import javax.activation.MimetypesFileTypeMap;

public class EchoServer {
	public static void main(String[] args) {
		try {
			ServerSocket server = new ServerSocket(80);
			System.out.println("접속을 기다립니다.");
			while (true) {
				Socket sock = server.accept();
				EchoThread echoThread = new EchoThread(sock);
				echoThread.start();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

class EchoThread extends Thread {
	private String DEFAULT_FILE_NAME = "home.html";

	private Socket sock;

	public EchoThread(Socket sock) {
		this.sock = sock;
	}

	public void run() {
		System.out.println("WebServer Thread Created");
		BufferedReader br = null;
		DataOutputStream outputSteam = null;

		try {
			br = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			outputSteam = new DataOutputStream(sock.getOutputStream());

			String line = br.readLine();
			StringTokenizer token = new StringTokenizer(line);

			if (token.nextToken().equals("GET")) {
				String fileName = token.nextToken();
				if (fileName.startsWith("/") == true) {
					if (fileName.length() > 1) {
						fileName = fileName.substring(1);
					} else {
						fileName = DEFAULT_FILE_NAME;
					}
				}

				String path = "C:\\Users\\leeJH\\Desktop\\W\\practice\\Socket Practice\\src\\echoserver\\";
				// path 설정해야함
				File file = new File(path + fileName);
				if (file.exists()) {
					String mimeType = new MimetypesFileTypeMap().getContentType(file);
					int maxBytes = 1048576;
					byte[] maxFileBytes = new byte[maxBytes];
					long numOfBytes = (long) file.length();
					FileInputStream inFile = new FileInputStream(file);

					outputSteam.writeBytes("HTTP/1.1 200 OK\r\n");
					outputSteam.writeBytes("Content-Type: " + mimeType + "\r\n");
					outputSteam.writeBytes("Content-Length: " + numOfBytes + "\r\n");
					outputSteam.writeBytes("Connection: close\r\n");
					outputSteam.writeBytes("\r\n");

					while (numOfBytes > maxBytes) {
						inFile.read(maxFileBytes);
						outputSteam.write(maxFileBytes, 0, maxBytes);

						numOfBytes -= maxBytes;
						System.out.println("byte : " + numOfBytes);
					}
					if (numOfBytes < maxBytes) {
						byte[] lessThenMax = new byte[(int) numOfBytes];
						System.out.println("rest : " + numOfBytes);
						inFile.read(lessThenMax);
						outputSteam.write(lessThenMax, 0, (int) numOfBytes);
					}

				} else {
					System.out.println("Requested File Not Found : " + file);
					outputSteam.writeBytes("HTTP/1.1 404 Not Found \r\n");
					outputSteam.writeBytes("Content-Type: text/html \r\n");
					outputSteam.writeBytes("Connection: close\r\n");
					outputSteam.writeBytes("\r\n");
				}
			} else {
				System.out.println("Bad Request");
				outputSteam.writeBytes("HTTP/1.1 400 Bad Request Message \r\n");
				outputSteam.writeBytes("Content-Type: text/html \r\n");
				outputSteam.writeBytes("Connection: close\r\n");
				outputSteam.writeBytes("\r\n");
			}

			sock.close();
			System.out.println("Connection Closed");
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}